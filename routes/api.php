<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'address'], function(){
    Route::get('{address}', 'Addresses\Show');
    Route::post('show-many', 'BatchAddresses\Index');
});

Route::group(['prefix' => 'agencies'], function() {
    Route::get('/', 'Agencies\Index');
    Route::get('{agency}', 'Agencies\Show');
});

Route::group(['prefix' => 'coupons'], function(){
    Route::get('/', 'Coupons\Index');
    Route::get('/{coupom}', 'Coupons\Show');
    Route::post('show-many', 'BatchCoupons\Index');
});

Route::group(['prefix' => 'customers'], function() {
    Route::get('/', 'Customers\Index');
    Route::get('{customer}', 'Customers\Show');
    Route::get('{customer}/addresses', 'CustomerAddresses\Index');
    Route::get('{customer}/status', 'CustomerStatus\Index');
    Route::get('{customer}/phones', 'CustomerPhones\Index');
    Route::get('{customer}/shippings', 'CustomerShippings\Index');
    Route::get('{customer}/coupons', 'CustomerCoupons\Index');
    Route::get('{customer}/role', 'CustomerRole\Index');
    Route::get('{customer}/limits', 'CustomerLimits\Index');
    Route::get('{customer}/payment-gateways', 'CustomerPaymentGateways\Index');
    Route::post('show-many', 'BatchCustomers\Index');
});

Route::group(['prefix' => 'payment-gateways'], function(){
    Route::get('/', 'PaymentGateways\Index');
    Route::get('/{payment_gateway}', 'PaymentGateways\Show');
});

Route::group(['prefix' => 'phones'], function() {
    Route::get('{phone}', 'Phones\Show');
});

Route::group(['prefix' => 'roles'], function(){
    Route::get('/', 'Roles\Index');
    Route::get('{role}', 'Roles\Show');
});

Route::group(['prefix' => 'services-shipping'], function(){
    Route::get('/', 'ServicesShipping\Index');
    Route::get('{service}', 'ServicesShipping\Show');
});

Route::group(['prefix' => 'shippings'], function() {
    Route::get('/', 'Shippings\Index');
    Route::get('{shipping}', 'Shippings\Show');
    Route::get('{shipping}/conciliations', 'Conciliations\Index');
});

Route::group(['prefix' => 'shipping-companies'], function() {
    Route::get('{company}', 'ShippingCompanies\Show');
});

Route::group(['prefix' => 'status'], function() {
    Route::get('/', 'Status\Index');
    Route::get('{status}', 'Status\Show');
});

Route::group(['prefix' => 'graphql'], function(){
    Route::post('customers-shippings', 'CustomersShippings\Index');
    Route::post('customers-coupons', 'CustomersCoupons\Index');
    Route::post('customers-addresses', 'CustomersAddresses\Index');
});