<?php

namespace App\Repositories;

use App\Repositories\Interfaces\CouponRepository;

class CouponEloquentRepository extends EloquentRepository implements CouponRepository
{
    public function getByIds(Array $slugs)
    {
        return $this->query()
            ->whereIn('slug', $slugs)
            ->get();
    }

    public function getByCustomers(Array $customers_id)
    {
        return $this->query()
            ->join('user_coupons', 'user_coupons.coupon', '=', 'coupons.coupon')
            ->whereIn('user_coupons.user_id', $customers_id)
            ->get();
    }
}