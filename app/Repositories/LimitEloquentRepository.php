<?php

namespace App\Repositories;

use App\Repositories\Interfaces\LimitRepository;

class LimitEloquentRepository extends EloquentRepository implements LimitRepository
{
    public function getByCustomer(String $customer_id)
    {
        return $this->query()
            ->where('user_id', $customer_id)
            ->first();
    }
}