<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RoleRepository;

class RoleEloquentRepository extends EloquentRepository implements RoleRepository
{
    public function getByCustomer(String $customer_id)
    {
        return $this->query()
            ->join('users_roles', 'users_roles.role_id', '=', 'roles.id')
            ->where('users_roles.user_id', $customer_id)
            ->select('roles.*')
            ->get();
    }
}