<?php

namespace App\Repositories;

use App\Repositories\Interfaces\AddressRepository;

class AddressEloquentRepository extends EloquentRepository implements AddressRepository
{
    public function getByCustomer(String $customer_id)
    {
        return $this->query()
            ->join('users_addresses', 'users_addresses.address_id', '=', 'addresses.id')
            ->where('users_addresses.user_id', $customer_id)
            ->select('addresses.*')
            ->get();
    }

    public function getByIds(Array $ids)
    {
        return $this->query()
            ->whereIn('id', $ids)
            ->get();
    }

    public function getByCustomers(Array $customers_id)
    {
        return $this->query()
            ->join('users_addresses', 'users_addresses.address_id', '=', 'addresses.id')
            ->whereIn('users_addresses.user_id', $customers_id)
            ->get();
    }
}