<?php

namespace App\Repositories\Interfaces;

interface LimitRepository
{
    public function getByCustomer(String $customer_id);
}