<?php

namespace App\Repositories\Interfaces;

interface CouponRepository
{
    public function getByIds(Array $slugs);

    public function getByCustomers(Array $customers_id);
}