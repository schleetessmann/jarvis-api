<?php

namespace App\Repositories\Interfaces;

interface PaymentGatewayRepository
{
    public function getByCustomerId(String $customer_id);
}