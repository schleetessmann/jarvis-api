<?php

namespace App\Repositories\Interfaces;

interface AddressRepository
{
    public function getByCustomer(String $customer_id);

    public function getByIds(Array $ids);

    public function getByCustomers(Array $customers_id);
}