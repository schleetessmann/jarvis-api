<?php

namespace App\Repositories\Interfaces;

interface CustomerCoupomRepository
{
    public function getCouponsByCustomerId(String $customer_id);
}