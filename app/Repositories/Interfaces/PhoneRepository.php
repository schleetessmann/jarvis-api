<?php

namespace App\Repositories\Interfaces;

interface PhoneRepository
{
    public function getByCustomer(String $customer_id);
}