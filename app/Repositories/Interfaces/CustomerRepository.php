<?php

namespace App\Repositories\Interfaces;

interface CustomerRepository
{
    public function getByIds(Array $ids);
}