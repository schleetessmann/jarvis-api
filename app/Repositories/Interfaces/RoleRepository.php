<?php

namespace App\Repositories\Interfaces;

interface RoleRepository
{
    public function getByCustomer(String $customer_id);
}