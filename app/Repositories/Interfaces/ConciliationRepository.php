<?php

namespace App\Repositories\Interfaces;

interface ConciliationRepository
{
    public function getByShippingId(String $shipping_id);
}