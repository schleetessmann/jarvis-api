<?php

namespace App\Repositories\Interfaces;

interface ShippingRepository
{
    public function getByCustomer(String $customer_id, Int $limit = 30);

    public function getByCustomers(Array $customers_id);
}