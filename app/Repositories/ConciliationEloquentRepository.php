<?php

namespace App\Repositories;

use App\Repositories\Interfaces\ConciliationRepository;

class ConciliationEloquentRepository extends EloquentRepository implements ConciliationRepository
{
    public function getByShippingId(String $shipping_id)
    {
        return $this->query()
            ->where('order_id', $shipping_id)
            ->get();
    }
}