<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

abstract class EloquentRepository
{
    /**
     * @var Model
     */
    private $model;

    /**
     * @var Builder
     */
    private $queryBuilder;

    /**
     * DatabaseRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return $this->model->query();
    }

    /**
     * @return object|null
     */
    public function get()
    {
        return $this->model->get();
    }

     /**
     * @param String $id
     * @return Collection
     */
    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * @return Collection
     */
    public function first()
    {
        return $this->model->first();
    }

    public function paginate($limit = 25)
    {
        return $this->model->paginate($limit);
    }

    /**
     * @param array $columns
     * @return Collection
     */
    public function all($columns = ['*'])
    {
        return $this->model->all($columns);
    }

    // create a new record in the database
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    // update record in the database
    public function update(array $data, $id)
    {
        $record = $this->find($id);
        return $record->update($data);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }
}
