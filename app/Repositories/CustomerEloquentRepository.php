<?php

namespace App\Repositories;

use App\Repositories\Interfaces\CustomerRepository;

class CustomerEloquentRepository extends EloquentRepository implements CustomerRepository
{
    public function getByIds(Array $ids)
    {
        return $this->query()
            ->whereIn('id', $ids)
            ->get();
    }
}