<?php

namespace App\Repositories;

use App\Repositories\Interfaces\ShippingRepository;

class ShippingEloquentRepository extends EloquentRepository implements ShippingRepository
{
    public function getByCustomer(String $customer_id, Int $limit = 30)
    {
        return $this->query()
            ->where('user_id', $customer_id)
            ->paginate();
    }

    public function getByCustomers(Array $customers_id)
    {
        return  $this->query()
            ->whereIn('user_id', $customers_id)
            ->where('canceled_at', '!=', null)
            ->where('deleted_at', '!=', null)
            ->get();
    }
}