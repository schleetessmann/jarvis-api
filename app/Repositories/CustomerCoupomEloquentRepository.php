<?php

namespace App\Repositories;

use App\Repositories\Interfaces\CustomerCoupomRepository;

class CustomerCoupomEloquentRepository extends EloquentRepository implements CustomerCoupomRepository
{
    public function getCouponsByCustomerId(String $customer_id)
    {
        return $this->query()
            ->join('coupons', 'coupons.coupon', '=', 'user_coupons.coupon')
            ->where('user_id', $customer_id)
            ->get();
    }
}