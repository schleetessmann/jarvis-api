<?php

namespace App\Repositories;

use App\Repositories\Interfaces\PhoneRepository;

class PhoneEloquentRepository extends EloquentRepository implements PhoneRepository
{
    public function getByCustomer(String $customer_id)
    {
        return $this->query()
            ->join('users_phones', 'users_phones.phone_id', '=', 'phones.id')
            ->where('users_phones.user_id', $customer_id)
            ->select('phones.*')
            ->get();
    }
}