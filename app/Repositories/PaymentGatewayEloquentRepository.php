<?php

namespace App\Repositories;

use App\Repositories\Interfaces\PaymentGatewayRepository;

class PaymentGatewayEloquentRepository extends EloquentRepository implements PaymentGatewayRepository
{
    public function getByCustomerId(String $customer_id)
    {
        return $this->query()
            ->join('users_payment_gateways', 'users_payment_gateways.gateway_id', '=', 'payment_gateways.id')
            ->where('users_payment_gateways.user_id', $customer_id)
            ->select('payment_gateways.*')
            ->get();
    }
}