<?php

namespace App\Providers;

use App\Models\Address;
use App\Models\Agency;
use App\Models\Conciliation;
use App\Models\Coupom;
use App\Repositories\CustomerEloquentRepository;
use App\Repositories\Interfaces\CustomerRepository;
use Illuminate\Support\ServiceProvider;
use App\Models\Customer;
use App\Models\CustomerCoupom;
use App\Models\Limit;
use App\Models\PaymentGateway;
use App\Models\Phone;
use App\Models\Role;
use App\Models\ServiceShipping;
use App\Models\Shipping;
use App\Models\ShippingCompany;
use App\Models\Status;
use App\Repositories\AddressEloquentRepository;
use App\Repositories\AgencyEloquentRepository;
use App\Repositories\ConciliationEloquentRepository;
use App\Repositories\CouponEloquentRepository;
use App\Repositories\CustomerCoupomEloquentRepository;
use App\Repositories\CustomerStatusEloquentRepository;
use App\Repositories\Interfaces\AddressRepository;
use App\Repositories\Interfaces\AgencyRepository;
use App\Repositories\Interfaces\ConciliationRepository;
use App\Repositories\Interfaces\CouponRepository;
use App\Repositories\Interfaces\CustomerCoupomRepository;
use App\Repositories\Interfaces\CustomerStatusRepository;
use App\Repositories\Interfaces\LimitRepository;
use App\Repositories\Interfaces\PaymentGatewayRepository;
use App\Repositories\Interfaces\PhoneRepository;
use App\Repositories\Interfaces\RoleRepository;
use App\Repositories\Interfaces\ServiceShippingRepository;
use App\Repositories\Interfaces\ShippingCompanyRepository;
use App\Repositories\Interfaces\ShippingRepository;
use App\Repositories\LimitEloquentRepository;
use App\Repositories\PaymentGatewayEloquentRepository;
use App\Repositories\PhoneEloquentRepository;
use App\Repositories\RoleEloquentRepository;
use App\Repositories\ServiceShippingEloquentRepository;
use App\Repositories\ShippingCompanyEloquentRepository;
use App\Repositories\ShippingEloquentRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(AddressRepository::class, function ($app) {
            return new AddressEloquentRepository(new Address());
        });

        $this->app->singleton(AgencyRepository::class, function ($app) {
            return new AgencyEloquentRepository(new Agency());
        });

        $this->app->singleton(ConciliationRepository::class, function ($app) {
            return new ConciliationEloquentRepository(new Conciliation());
        });

        $this->app->singleton(CouponRepository::class, function ($app) {
            return new CouponEloquentRepository(new Coupom());
        });

        $this->app->singleton(CustomerRepository::class, function ($app) {
            return new CustomerEloquentRepository(new Customer());
        });

        $this->app->singleton(CustomerCoupomRepository::class, function ($app) {
            return new CustomerCoupomEloquentRepository(new CustomerCoupom());
        });

        $this->app->singleton(CustomerStatusRepository::class, function ($app) {
            return new CustomerStatusEloquentRepository(new Status());
        });

        $this->app->singleton(LimitRepository::class, function ($app) {
            return new LimitEloquentRepository(new Limit());
        });

        $this->app->singleton(PaymentGatewayRepository::class, function ($app) {
            return new PaymentGatewayEloquentRepository(new PaymentGateway());
        });

        $this->app->singleton(PhoneRepository::class, function ($app) {
            return new PhoneEloquentRepository(new Phone());
        });

        $this->app->singleton(RoleRepository::class, function ($app) {
            return new RoleEloquentRepository(new Role());
        });

        $this->app->singleton(ServiceShippingRepository::class, function ($app) {
            return new ServiceShippingEloquentRepository(new ServiceShipping());
        });

        $this->app->singleton(ShippingRepository::class, function ($app) {
            return new ShippingEloquentRepository(new Shipping());
        });

        $this->app->singleton(ShippingCompanyRepository::class, function ($app) {
            return new ShippingCompanyEloquentRepository(new ShippingCompany());
        });
    }
}