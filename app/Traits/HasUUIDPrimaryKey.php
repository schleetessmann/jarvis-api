<?php

namespace App\Traits;

use Ramsey\Uuid\Uuid;

trait HasUUIDPrimaryKey
{
    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Boot function for using with Model Events
     *
     * @return void
     */
    public static function bootHasUUIDPrimaryKey()
    {
        static::creating(function ($model) {
            $model->attributes[$model->getKeyName()] = Uuid::uuid4()->toString();
        });

        static::updating(function ($model) {
            $keyName = $model->getKeyName();

            $original = $model->getOriginal($keyName);

            if ($original !== $model->attributes[$keyName]) {
                $model->attributes[$keyName] = $original;
            }
        });
    }
}
