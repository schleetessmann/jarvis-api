<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingCompany extends Model
{
    protected $table = 'shipping_companies';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'class',
        'credentials',
        'document',
        'email',
        'use_own_contract'
    ];
}
