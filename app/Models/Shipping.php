<?php

namespace App\Models;

use App\Traits\HasUUIDPrimaryKey;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    use HasUUIDPrimaryKey;
    
    protected $table = 'orders';

    const PREFIX = 'ORD';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'purchase_id',
        'rate',
        'cost',
        'platform',
        'delivery_date',
        'deleted_at',
    ];

    /**
     * @return string
     */
    public function getProtocolAttribute()
    {
        return self::PREFIX . '-' . $this->created_at->format('Ym') . $this->attributes['protocol'];
    }
}
