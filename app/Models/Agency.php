<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $table = 'shipping_agencies';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'count_notified_email',
        'count_notified_phone',
        'last_notified_email_at',
        'last_notified_phone_at'
    ];
}
