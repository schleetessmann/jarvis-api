<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentGateway extends Model
{
    protected $table = 'payment_gateways';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'class',
        'email',
        'address_id',
        'phone_id',
        'contact_id'
    ];

    protected $casts = [
        'credentials' => 'json'
    ];

}
