<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceShipping extends Model
{
    protected $table = 'shipping_services';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
