<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $table = 'phones';

    protected $hidden = [
        'confirmed_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
