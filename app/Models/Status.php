<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'user_status';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
