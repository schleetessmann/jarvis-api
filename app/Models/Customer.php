<?php

namespace App\Models;

use App\Traits\HasUUIDPrimaryKey;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasUUIDPrimaryKey;

    const PREFIX = 'USR';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'access_at',
        'birthdate',
        'email_confirmed_at',
        'deleted_at',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'access_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @return string
     */
    public function getProtocolAttribute()
    {
        return self::PREFIX . '-' . $this->created_at->format('Ym') . $this->attributes['protocol'];
    }
}
