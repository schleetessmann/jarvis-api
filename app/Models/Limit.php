<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Limit extends Model
{
    protected $table = 'user_limits';

    protected $hidden = [
        'created_at',
        'updated_at',
        'user_id'
    ];
}
