<?php

namespace App\Http\Controllers\CustomerShippings;

use App\Http\Controllers\Controller;
use App\Http\Resources\Shippings;
use App\Models\Customer;
use App\Repositories\Interfaces\ShippingRepository;

class Index extends Controller
{
    const LIMIT_PAGINATE = 30;

    public function __invoke(ShippingRepository $shippingRepository, Customer $customer)
    {
        try {
            return new Shippings(
                $shippingRepository->getByCustomer(
                    $customer->id, 
                    self::LIMIT_PAGINATE
                )
            );
          
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
