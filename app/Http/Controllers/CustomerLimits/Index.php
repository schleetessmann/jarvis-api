<?php

namespace App\Http\Controllers\CustomerLimits;

use App\Http\Controllers\Controller;
use App\Http\Resources\Limit;
use App\Models\Customer;
use App\Repositories\Interfaces\LimitRepository;

class Index extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(LimitRepository $limitRepository, Customer $customer)
    {
        try {
            return new Limit(
                $limitRepository->getByCustomer($customer->id)
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
