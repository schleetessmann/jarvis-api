<?php

namespace App\Http\Controllers\CustomerRole;

use App\Http\Controllers\Controller;
use App\Http\Resources\Role;
use App\Models\Customer;
use App\Repositories\Interfaces\RoleRepository;

class Index extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RoleRepository $roleRepository, Customer $customer)
    {
        try {
            return new Role(
                $roleRepository->getByCustomer($customer->id)
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
