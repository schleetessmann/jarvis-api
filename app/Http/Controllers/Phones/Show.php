<?php

namespace App\Http\Controllers\Phones;

use App\Http\Controllers\Controller;
use App\Http\Resources\Phone as ResourcesPhone;
use App\Models\Phone;

class Show extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Phone $phone)
    {
        try {
            return new ResourcesPhone($phone);
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
