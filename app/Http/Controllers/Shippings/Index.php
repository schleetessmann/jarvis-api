<?php

namespace App\Http\Controllers\Shippings;

use App\Http\Controllers\Controller;
use App\Http\Resources\Shippings;
use App\Repositories\Interfaces\ShippingRepository;

class Index extends Controller
{
    const LIMIT_PAGINATE = 30;

    public function __invoke(ShippingRepository $shippingRepository)
    {
        try {
            return new Shippings(
                $shippingRepository->paginate(self::LIMIT_PAGINATE)
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
