<?php

namespace App\Http\Controllers\Shippings;

use App\Http\Controllers\Controller;
use App\Http\Resources\Shipping as ResourcesShipping;
use App\Models\Shipping;

class Show extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Shipping $shipping)
    {
        try {
            return new ResourcesShipping($shipping);
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
