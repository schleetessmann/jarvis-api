<?php

namespace App\Http\Controllers\Agencies;

use App\Http\Controllers\Controller;
use App\Http\Resources\Agency as ResourcesAgency;
use App\Models\Agency;

class Show extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Agency $agency)
    {
        try {
            return new ResourcesAgency($agency);
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
