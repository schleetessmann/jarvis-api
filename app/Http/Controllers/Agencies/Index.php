<?php

namespace App\Http\Controllers\Agencies;

use App\Http\Controllers\Controller;
use App\Http\Resources\Agencies;
use App\Http\Resources\Agency;
use App\Repositories\Interfaces\AgencyRepository;

class Index extends Controller
{
    const LIMIT_PAGINATE = 30;
    
    public function __invoke(AgencyRepository $agencyRepository)
    {
        try {
           return new Agencies(
                $agencyRepository->paginate(self::LIMIT_PAGINATE)
           );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
