<?php

namespace App\Http\Controllers\CustomerPhones;

use App\Http\Controllers\Controller;
use App\Http\Resources\Phones;
use App\Models\Customer;
use App\Repositories\Interfaces\PhoneRepository;

class Index extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(PhoneRepository $phoneRepository, Customer $customer)
    {
        try {
            return new Phones(
                $phoneRepository->getByCustomer($customer->id)
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
