<?php

namespace App\Http\Controllers\Roles;

use App\Http\Controllers\Controller;
use App\Http\Resources\Roles;
use App\Repositories\Interfaces\RoleRepository;
use Illuminate\Http\Request;

class Index extends Controller
{
    const LIMIT_PAGINATE = 30;

    public function __invoke(Request $request, RoleRepository $roleRepository)
    {
        try {
            return new Roles(
                $roleRepository->paginate(self::LIMIT_PAGINATE)
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
