<?php

namespace App\Http\Controllers\Roles;

use App\Http\Controllers\Controller;
use App\Http\Resources\Role as ResourceRole;
use App\Models\Role;

class Show extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Role $role)
    {
        try {
            return new ResourceRole($role);
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
