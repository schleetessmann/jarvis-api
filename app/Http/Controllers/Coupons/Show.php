<?php

namespace App\Http\Controllers\Coupons;

use App\Http\Controllers\Controller;
use App\Http\Resources\Coupom as ResourcesCoupom;
use App\Repositories\Interfaces\CoupomRepository;

class Show extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(CoupomRepository $coupomRepository, $coupom)
    {
        try {
            return new ResourcesCoupom(
                $coupomRepository->query()
                    ->where('slug', $coupom)
                    ->first()
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
