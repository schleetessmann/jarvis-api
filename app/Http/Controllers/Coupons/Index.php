<?php

namespace App\Http\Controllers\Coupons;

use App\Http\Controllers\Controller;
use App\Http\Resources\Coupons;
use App\Repositories\Interfaces\CoupomRepository;

class Index extends Controller
{
    const LIMIT_PAGINATE = 30;
    
    public function __invoke(CoupomRepository $coupomRepository)
    {
        try {
          return new Coupons(
            $coupomRepository->paginate(self::LIMIT_PAGINATE)
          );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
