<?php

namespace App\Http\Controllers\CustomerPaymentGateways;

use App\Http\Controllers\Controller;
use App\Http\Resources\paymentGateways;
use App\Models\Customer;
use App\Repositories\Interfaces\PaymentGatewayRepository;

class Index extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(PaymentGatewayRepository $paymentGatewayRepository, Customer $customer)
    {
        try {
            return new paymentGateways(
                $paymentGatewayRepository->getByCustomerId($customer->id)
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
