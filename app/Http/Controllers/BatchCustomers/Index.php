<?php

namespace App\Http\Controllers\BatchCustomers;

use App\Http\Controllers\Controller;
use App\Http\Resources\Customers;
use App\Repositories\Interfaces\CustomerRepository;
use Illuminate\Http\Request;

class Index extends Controller
{
    
    public function __invoke(Request $request, CustomerRepository $customerRepository)
    {
        try {
            
            $customersID = $request->get('customers', []);

            if (empty($customersID)) {
                return response()->json([
                    'error' => 'Informar os "ids" dos clientes'
                ]);
            }

            return new Customers(
                $customerRepository->getByIds($customersID)
            );

        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
