<?php

namespace App\Http\Controllers\CustomersShippings;

use App\Http\Controllers\Controller;
use App\Http\Resources\Shippings;
use App\Repositories\Interfaces\ShippingRepository;
use App\Services\GroupObjectsByCustomer;
use Illuminate\Http\Request;

class Index extends Controller
{
    const LIMIT_PAGINATE = 30;

    public function __invoke(Request $request, ShippingRepository $shippingRepository)
    {
        try {

            $customersID = $request->get('customers', []);

            if (empty($customersID)) {
                return response()->json([
                    'error' => 'Informar os "ids" dos clientes'
                ]);
            }

            $shippings = $shippingRepository->getByCustomers($customersID);

            $shippings = (new GroupObjectsByCustomer())->organize(
                $customersID, 
                $shippings
            );

            return response()->json($shippings, 200);
           
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
