<?php

namespace App\Http\Controllers\Status;

use App\Http\Controllers\Controller;
use App\Http\Resources\StatusCollection;
use App\Repositories\Interfaces\CustomerStatusRepository;

class Index extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(CustomerStatusRepository $statusRepository)
    {
        try {
            return new StatusCollection(
                $statusRepository->all()
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
