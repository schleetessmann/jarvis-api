<?php

namespace App\Http\Controllers\Status;

use App\Http\Controllers\Controller;
use App\Http\Resources\Status as ResourcesStatus;
use App\Models\Status;

class Show extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Status $status)
    {
        try {
            return new ResourcesStatus($status);
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
