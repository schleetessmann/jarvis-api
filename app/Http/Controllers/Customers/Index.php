<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Http\Resources\Customers;
use App\Repositories\Interfaces\CustomerRepository;

class Index extends Controller
{
    const LIMIT_PAGINATE = 30;
    
    public function __invoke(CustomerRepository $customerRepository)
    {
        try {
            return new Customers(
                $customerRepository->paginate(self::LIMIT_PAGINATE)
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
