<?php

namespace App\Http\Controllers\ServicesShipping;

use App\Http\Controllers\Controller;
use App\Http\Resources\ServicesShipping;
use App\Repositories\Interfaces\ServiceShippingRepository;

class Index extends Controller
{
    const LIMIT_PAGINATE = 30;
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ServiceShippingRepository $serviceShippingRepository)
    {
        try {
            return new ServicesShipping(
                $serviceShippingRepository->paginate(self::LIMIT_PAGINATE)
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
