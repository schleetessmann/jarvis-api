<?php

namespace App\Http\Controllers\ServicesShipping;

use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceShipping as ResourcesServiceShipping;
use App\Models\ServiceShipping;

class Show extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ServiceShipping $service)
    {
        try {
            return new ResourcesServiceShipping($service);
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
