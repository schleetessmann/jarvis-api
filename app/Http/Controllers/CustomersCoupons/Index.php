<?php

namespace App\Http\Controllers\CustomersCoupons;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\CouponRepository;
use App\Services\GroupObjectsByCustomer;
use Illuminate\Http\Request;

class Index extends Controller
{
    const LIMIT_PAGINATE = 30;

    public function __invoke(Request $request, CouponRepository $coupomRepository)
    {
        try {

            $customersID = $request->get('customers', []);

            if (empty($customersID)) {
                return response()->json([
                    'error' => 'Informar os "ids" dos clientes'
                ]);
            }

            $coupons = $coupomRepository->getByCustomers($customersID);

            $coupons = (new GroupObjectsByCustomer())->organize(
                $customersID, 
                $coupons
            );

            return response()->json($coupons, 200);
           
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}