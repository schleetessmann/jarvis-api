<?php

namespace App\Http\Controllers\PaymentGateways;

use App\Http\Controllers\Controller;
use App\Http\Resources\paymentGateways;
use App\Repositories\Interfaces\PaymentGatewayRepository;

class Index extends Controller
{
    const LIMIT_PAGINATE = 30;
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(PaymentGatewayRepository $paymentGatewayRepository)
    {
        try {
            return new paymentGateways(
                $paymentGatewayRepository->paginate(self::LIMIT_PAGINATE)
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
