<?php

namespace App\Http\Controllers\PaymentGateways;

use App\Http\Controllers\Controller;
use App\Http\Resources\PaymentGateway as ResourcesPaymentGateway;
use App\Models\PaymentGateway;

class Show extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(PaymentGateway $payment_gateway)
    {
        try {
            return new ResourcesPaymentGateway($payment_gateway);
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
