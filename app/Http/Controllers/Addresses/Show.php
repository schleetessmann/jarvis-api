<?php

namespace App\Http\Controllers\Addresses;

use App\Http\Controllers\Controller;
use App\Http\Resources\Address as ResourcesAddress;
use App\Models\Address;

class Show extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Address $address)
    {
        try {
            return new ResourcesAddress($address);
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
