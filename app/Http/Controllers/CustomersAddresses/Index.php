<?php

namespace App\Http\Controllers\CustomersAddresses;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\AddressRepository;
use App\Services\GroupObjectsByCustomer;
use Illuminate\Http\Request;

class Index extends Controller
{
    const LIMIT_PAGINATE = 30;

    public function __invoke(Request $request, AddressRepository $addressRepository)
    {
        try {

            $customersID = $request->get('customers', []);

            if (empty($customersID)) {
                return response()->json([
                    'error' => 'Informar os "ids" dos clientes'
                ]);
            }

            $addresses = $addressRepository->getByCustomers($customersID);

            $addresses = (new GroupObjectsByCustomer())->organize(
                $customersID, 
                $addresses
            );

            return response()->json($addresses, 200);
           
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}