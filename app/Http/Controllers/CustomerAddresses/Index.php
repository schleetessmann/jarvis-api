<?php

namespace App\Http\Controllers\CustomerAddresses;

use App\Http\Controllers\Controller;
use App\Http\Resources\Address as ResourcesAddress;
use App\Models\Customer;
use App\Repositories\Interfaces\AddressRepository;
class Index extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(AddressRepository $addressRepository, Customer $customer)
    {
        try {
            return new ResourcesAddress(
                $addressRepository->getByCustomer($customer->id)
            );

        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
