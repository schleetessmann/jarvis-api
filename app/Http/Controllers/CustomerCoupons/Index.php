<?php

namespace App\Http\Controllers\CustomerCoupons;

use App\Http\Controllers\Controller;
use App\Http\Resources\Coupons;
use App\Models\Customer;
use App\Repositories\Interfaces\CustomerCoupomRepository;

class Index extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(CustomerCoupomRepository $coupomRepository, Customer $customer)
    {
        try {
            return new Coupons(
                $coupomRepository->getCouponsByCustomerId($customer->id)
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
