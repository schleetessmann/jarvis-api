<?php

namespace App\Http\Controllers\ShippingCompanies;

use App\Http\Controllers\Controller;
use App\Http\Resources\ShippingCompany as ResourcesShippingCompany;
use App\Models\ShippingCompany;

class Show extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ShippingCompany $company)
    {
        try {
            return new ResourcesShippingCompany($company);
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
