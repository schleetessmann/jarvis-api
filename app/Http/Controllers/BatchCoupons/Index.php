<?php

namespace App\Http\Controllers\BatchCoupons;

use App\Http\Controllers\Controller;
use App\Http\Resources\Coupons;
use App\Repositories\Interfaces\CoupomRepository;
use Illuminate\Http\Request;

class Index extends Controller
{
    
    public function __invoke(Request $request, CoupomRepository $coupomRepository)
    {
        try {
            
            $couponsSlugs = $request->get('slugs', []);

            if (empty($couponsSlugs)) {
                return response()->json([
                    'error' => 'Informar os "slugs" dos coupons'
                ]);
            }

            return new Coupons(
                $coupomRepository->getByIds($couponsSlugs)
            );

        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
