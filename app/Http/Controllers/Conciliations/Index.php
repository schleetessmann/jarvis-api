<?php

namespace App\Http\Controllers\Conciliations;

use App\Http\Controllers\Controller;
use App\Http\Resources\Conciliations;
use App\Models\Shipping;
use App\Repositories\Interfaces\ConciliationRepository;

class Index extends Controller
{
    
    public function __invoke(ConciliationRepository $conciliationRepository, Shipping $shipping)
    {
        try {
            return new Conciliations(
                $conciliationRepository->getByShippingId($shipping->id)
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
