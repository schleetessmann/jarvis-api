<?php

namespace App\Http\Controllers\BatchAddresses;

use App\Http\Controllers\Controller;
use App\Http\Resources\Addresses;
use App\Repositories\Interfaces\AddressRepository;
use Illuminate\Http\Request;

class Index extends Controller
{
    
    public function __invoke(Request $request, AddressRepository $AddressRepository)
    {
        try {
            
            $addressesIds = $request->get('addresses', []);

            if (empty($addressesIds)) {
                return response()->json([
                    'error' => 'Informar os "ids" dos endereços'
                ]);
            }

            return new Addresses(
                $AddressRepository->getByIds($addressesIds)
            );

        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
