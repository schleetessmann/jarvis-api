<?php

namespace App\Http\Controllers\CustomerStatus;

use App\Http\Controllers\Controller;
use App\Http\Resources\Status;
use App\Models\Customer;
use App\Repositories\Interfaces\CustomerStatusRepository;

class Index extends Controller
{
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(CustomerStatusRepository $statusRepository, Customer $customer)
    {
        try {
            return new Status(
                $statusRepository->query()
                    ->where('id', $customer->status_id)
                    ->first()
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
}
