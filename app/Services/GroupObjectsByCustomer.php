<?php

namespace App\Services;

class GroupObjectsByCustomer
{
    public function organize(Array $customers, $data)
    {
        $response = [];
        foreach ($customers as $customer) {
            $response[$customer] = [];
            foreach($data as $item) {
                if ($item->user_id === $customer) {
                    $response[$customer][] = $item->getAttributes();
                }
            }
        }
        return $response;
    }
}
